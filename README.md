# Blockchain

This respository contains all of my blockchain projects. 

### Simple Blockchain Python Projects

A simple python program that simulates and implements a self made blockchain. 

### Blockchain App

A web application that implements a blockchain, transactions, and mining. 

### Full Stack Blockchain Web App

A full stack blockchain web app that deploys Ethereum smart contracts to a Marketplace interface where users can buy and sell items with Ethereum cryptocurrency. 

### Other files

README.md -> This files

journal.md -> Links to journals

sources.md -> Links to inspiration sources
