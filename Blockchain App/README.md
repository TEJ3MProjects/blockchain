# Blockchain App

This web application implements a self made blockchain that can handle transactions, mining, and works with a web interface and HTTP request functionalities. 

## Tools Used:
* Python
* HTTP Requests
* Postman
* Heroku 
* HTML/CSS/JS
* Flask
* Json, UUID

## Next Steps: 

* Add more features and functionalities to the blockchain
* Deploy with Kubernetes or use a UWSGI development server
* Add more responsive Typescript/Javascript features in the web app frontend interface
