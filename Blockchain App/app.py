# IMport flask and json dependencies, as well as the blockchain program
from blockchain import Blockchain
import json
from uuid import uuid4
from flask import Flask, request, Response, render_template

# Instantiate our Node
app = Flask(__name__)
app.config['JSONIFY_PRETTYPRINT_REGULAR'] = False

# Generate a unique address for the Node
node_indentifier = str(uuid4()).replace('-', '')

# Initialize the Blockchain
blockchain = Blockchain()

# Reroute to home
@app.route("/")
def home():
    return render_template("index.html")

# Reroute to /mine when user sends a GET request 
@app.route('/mine', methods=['GET'])
def mine():
    # Run the proof of work algorithm to get the last proof
    last_block = blockchain.last_block
    last_proof = last_block['proof']
    proof = blockchain.proof_of_work(last_proof)
    # The sender recieves a new block for getting the right proof
    blockchain.new_transaction(
        sender = "0",
        recipient=node_indentifier,
        amount = 1,
    )
    # Append the new block to the chain 
    previous_hash = blockchain.hash(last_block)
    block = blockchain.new_block(proof, previous_hash)
    # Send a json response to the GET request and a 200 OK status code
    response = {
        'message': "New Block Added",
        'index': block['index'],
        'transactions': block['transactions'],
        'proof': block['proof'],
        'previous_hash': block['previous_hash'],
    }
    return Response(json.dumps(response), content_type='application/json; charset=utf-8'), 200

# Reroute to /transactions/new when a post request is sent with a valid json transactikon
@app.route('/transactions/new', methods=['POST'])
def new_transaction():]
    # Get the requested transactions from the json request
    values = request.get_json(force=True)
    # Check that the required fields are in the POST data
    required = ['sender', 'recipient', 'amount']
    if not all(k in values for k in required):
        return 'Missing values', 400
    # Create a new transaction to the Blockchain
    index = blockchain.new_transaction(values['sender'], values['recipient'], values['amount'])
    response = {'message': f'Transaction will be added to Block {index}'}
    return Response(json.dumps(response), content_type='application/json; charset=utf-8'), 201

# Reroute to /chain when a GET request asks for the entire chain
@app.route('/chain', methods=['GET'])
def full_chain():
    # Print the chain as a json response to the user
    response = {
        'chain': blockchain.chain,
        'length': len(blockchain.chain),
    }
    return Response(json.dumps(response), content_type='application/json; charset=utf-8'), 200

# Reroute to /nodes/register when a new node is requested to be added to the blockchain
@app.route('/nodes/register', methods=['POST'])
def register_nodes():
    # Get the node values from the json
    values = request.get_json()
    nodes = values.get('nodes')
    # Add the nodes to the blockchain
    if nodes is None:
        return "Error: Please supply a valid list of nodes", 400
    for node in nodes:
        blockchain.register_node(node)
    # Return a success json response
    response = {
        'message': 'New nodes have been added',
        'total_nodes': list(blockchain.nodes),
    }
    return Response(json.dumps(response), content_type='application/json; charset=utf-8'), 201

# Reroute to /nodes/reslove when the user wants to update the main chain 
@app.route('/nodes/resolve', methods=['GET'])
def consensus():
    # Resolve any chain conflicts in the blockchain
    replaced = blockchain.resolve_conficts()
    # Return if the chain needed to be updated as a json success response
    if replaced:
        response = {
            'message': 'Our chain was replaced',
            'new_chain': blockchain.chain,
        }
    else:
        response = {
            'message': 'Our chain is is the longest',
            'new_chain': blockchain.chain,
        }
    return Response(json.dumps(response), content_type='application/json; charset=utf-8'), 200

# Run the flask app on port 5000 on run
if __name__ == "__main__":
    app.run(host = "0.0.0.0", port = 5000)