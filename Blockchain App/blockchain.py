# Import dependencies
import hashlib
import json
import requests
from textwrap import dedent
from time import time
from urllib.parse import urlparse

# Blockchain class that concatenates blocks or nodes
class Blockchain(object):
    def __init__(self):
        # Instance variables of transactions and nodes
        self.current_transactions = []
        self.chain = []
        self.nodes = set()
        # Create the genesis (first) block
        self.new_block(previous_hash = 1, proof = 100)

    # Create a new block in the blockchain
    def new_block(self, proof, previous_hash = None):
        block = {
            'index': len(self.chain) + 1,
            'timestamp': time(),
            'transactions': self.current_transactions,
            'proof': proof,
            'previous_hash': previous_hash or self.hash(self.chain[-1]),
        }
        # Reset the current transactions list
        self.current_transactions = []
        self.chain.append(block)
        return block

    # Adds a new transaction to the current transactions block
    def new_transaction(self, sender, recipient, amount):
        self.current_transactions.append({
            'sender': sender,
            'recipient': recipient,
            'amount': amount,
        })
        return self.last_block['index'] + 1

    """
    The proof of work algorithm is to find a number q such that 
    the hash of q x p (hash(pq)) contains 4 leading zeroes. 
    """
    def proof_of_work(self, last_proof):
        proof = 0
        while(self.valid_proof(last_proof, proof) is False):
            proof += 1
        return proof

    # Add a new node to the node network given the address
    def register_node(self, address):
        parsed_url = urlparse(address)
        self.nodes.add(parsed_url.netloc)

    # Determine if the chain of the Blockchain is valid
    def valid_chain(self, chain):
        last_block = chain[0]
        current_index = 1
        while(current_index < len(chain)):
            block = chain[current_index]
            #print(f'{last_block}')
            #print(f'{block}')
            #print("\n")
            # Check that the hash of the block is correct
            if(block['previous_hash'] != self.hash(last_block)):
                return False
            # Check that the proof of work is correct (the block was mined and not changed)
            if(not self.valid_proof(last_block['proof'], block['proof'])):
                return False
            last_block = block
            current_index += 1
        return True

    # The Consensus Algorithm resolves conflicts by replacing the main chain with the longest chain in the network 
    # Return whether the chain was replaced or not
    def resolve_conficts(self):
        neighbours = self.nodes
        new_chain = None
         # Find the max chain length
        max_length = len(self.chain)
        # Verify the chains from all the nodes in the network
        for node in neighbours:
            # Request the chain with a get request
            response = requests.get(f'http://{node}/chain')
            if(response.status_code == 200):
                length = response.json()['length']
                chain = response.json()['chain']
                # Check if the length is longer and the chain is valid, and save it
                if(length > max_length and self.valid_chain(chain)):
                    max_length = length
                    new_chain = chain
        # Replace the chain if a longer chain exists
        if(new_chain):
            self.chain = new_chain
            return True
        return False

    # Validates the proof of whether the hash of the proofs contains 4 leading zeroes
    @staticmethod
    def valid_proof(last_proof, proof):
        guess = f'{last_proof}{proof}'.encode()
        guess_hash = hashlib.sha256(guess).hexdigest()
        return guess_hash[:4] == '0000'

    # Static method that calculates the hash of the block using sh256
    @staticmethod
    def hash(block):
        # Sort the dictionary so the hashes are consistent
        block_string = json.dumps(block, sort_keys=True).encode()
        # Return the hash using sha256
        return hashlib.sha256(block_string).hexdigest()

    # Returns the last block in the main chain
    @property
    def last_block(self):
        return self.chain[-1]