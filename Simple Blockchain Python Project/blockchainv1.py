# import dependencies
import hashlib
import time

# Create each block of the blocchain
class Block(object):
    # Instance variables and initialization
    def __init__(self, index, proof_number, previous_hash, data, timestamp=None):
        # Contains an index, proof of work, previous hash, hash data, and timestamp
        self.index = index
        self.proof_number = proof_number
        self.previous_hash = previous_hash
        self.data = data
        self.timestamp = timestamp or time.time()
    @property
    # Method to compute the hash of the block using sha256
    def compute_hash(self):
        string_block = "{}{}{}{}{}".format(self.index, self.proof_number, self.previous_hash, self.data, self.timestamp)
        return hashlib.sha256(string_block.encode()).hexdigest()
    # Debug method to return the values in the block
    def __repr__(self):
        return "{}, {}, {}, {}, {}".format(self.index, self.proof_number, self.previous_hash, self.data, self.timestamp)
# Create the blockchain as the concatenation of many blocks
class BlockChain(object):
    # Initialization
    def __init__(self):
        # Contains a chain, data, nodes, and a static build method
        self.chain = []
        self.current_data = []
        self.nodes = set()
        self.build_genesis()
    # Static genesis method to build the initial block in the blockchain
    def build_genesis(self):
        self.build_block(proof_number = 0, previous_hash = 0)
    # Static method to create the first block in the blockchain
    def build_block(self, proof_number, previous_hash):
        block = Block(
            index = len(self.chain),
            proof_number = proof_number,
            previous_hash = previous_hash,
            data = self.current_data
        )
        self.current_data = []  
        self.chain.append(block)
        return block
    @staticmethod
    # Static method to confirm that the blocks have not been tampered with
    def confirm_validity(block, previous_block):
        if previous_block.index + 1 != block.index:
            return False
        elif previous_block.compute_hash != block.previous_hash:
            return False
        elif block.timestamp <= previous_block.timestamp:
            return False
        return True
    def chain_validity(self):
        last_block = self.latest_block
        sl_block = self.chain[-2]
        return confirm_validity(last_block, sl_block)
    # Get the data of the blockchain transactions
    def get_data(self, sender, receiver, amount):
        self.current_data.append({
            'sender': sender,
            'receiver': receiver,
            'amount': amount
        })
        return True 
    # Proof of work was not implemented in this version 
    @staticmethod
    def proof_of_work(last_proof):
        pass
    # Get the latest block
    @property
    def latest_block(self):
        return self.chain[-1]
    # Method to mine the block
    def block_mining(self, details_miner): 
        self.get_data(
            sender = "0", 
            receiver = details_miner,
            quantity = 1, 
        )
        # Get the last block, and add the new block to connect to last_block
        last_block = self.latest_block
        last_proof_number = last_block.proof_number
        proof_number = self.proof_of_work(last_proof_number)
        last_hash = last_block.compute_hash
        block = self.build_block(proof_number, last_hash)
        return vars(block)  
    # Method to get the formatted block object 
    @staticmethod
    def get_block_object(block_data):        
        return Block(
            block_data['index'],
            block_data['proof_number'],
            block_data['previous_hash'],
            block_data['data'],
            timestamp=block_data['timestamp']
        )

user = input("What is your name? ")
# Initialize the blockchain
blockchain = BlockChain()
# Test the blockchain mining algorithm
print("Mining starts... ")
print(blockchain.chain)
# Get the latest block, add a new block, and update the data
last_block = blockchain.latest_block
last_proof_number = last_block.proof_number
proof_number = blockchain.proof_of_work(last_proof_number)
blockchain.get_data(
    sender = "0", 
    receiver = user, 
    amount = 1, 
)
last_hash = last_block.compute_hash
block = blockchain.build_block(proof_number, last_hash)
print("Mining was successful!")
# Print out the added blockchain
print(blockchain.chain)