# Import dependencies
import hashlib
import json
from time import time
from urllib.parse import urlparse
import requests

# Blockchain class that concatenates blocks or nodes
class Blockchain:
    def __init__(self):
        # Instance variables of transactions and nodes
        self.current_transactions = []
        self.chain = []
        self.nodes = set()
        # Create the genesis (first) block
        self.new_block(previous_hash = '1', proof = 100)
    # Register the new node and add it to the network
    def register_node(self, address):
        parsed_url = urlparse(address)
        if parsed_url.netloc:
            self.nodes.add(parsed_url.netloc)
        elif parsed_url.path:
            # Accepts an URL (like 192.168.0.5:5000')
            self.nodes.add(parsed_url.path)
        else:
            raise ValueError('Invalid URL')
    # Validation method to check if a chain is valid
    def valid_chain(self, chain):
        last_block = chain[0]
        current_index = 1
        while current_index < len(chain):
            block = chain[current_index]
            #print(f'{last_block}')
            #print(f'{block}')
            #print("\n")
            # Check that the hash of the block is correct
            last_block_hash = self.hash(last_block)
            if block['previous_hash'] != last_block_hash:
                return False
            # Check that the proof of work is correct (the block was mined and not changed)
            if not self.valid_proof(last_block['proof'], block['proof'], last_block_hash):
                return False
            last_block = block
            current_index += 1
        return True
    # The consensus algorithm resolves conflicts by replacing the main chain with the longest chain in the network 
    # Return whether the chain was replaced or not
    def resolve_conflicts(self):
        neighbours = self.nodes
        new_chain = None
        # Find the max chain length
        max_length = len(self.chain)
        # Verify the chains from all the nodes in the network
        for node in neighbours:
            # Request the chain with a get request
            response = requests.get(f'http://{node}/chain')
            if response.status_code == 200:
                length = response.json()['length']
                chain = response.json()['chain']
                # Check if the length is longer and the chain is valid, and save it
                if length > max_length and self.valid_chain(chain):
                    max_length = length
                    new_chain = chain
        # Replace the chain if a longer chain exists
        if new_chain:
            self.chain = new_chain
            return True
        return False
    # Add a new block to the blockchain
    def new_block(self, proof, previous_hash):
        block = {
            'index': len(self.chain) + 1,
            'timestamp': time(),
            'transactions': self.current_transactions,
            'proof': proof,
            'previous_hash': previous_hash or self.hash(self.chain[-1]),
        }
        # Reset the current list of transactions and add the new block to the chain
        self.current_transactions = []
        self.chain.append(block)
        return block
    # Method that creates a transaction and adds a new mined block
    def new_transaction(self, sender, recipient, amount):
        self.current_transactions.append({
            'sender': sender,
            'recipient': recipient,
            'amount': amount,
        })
        return self.last_block['index'] + 1
    # Get the last block in the blockchain
    @property
    def last_block(self):
        return self.chain[-1]
    # Static method that calculates the hash of the block using sh256
    @staticmethod
    def hash(block):
        # Sort the dictionary so the hashes are consistent
        block_string = json.dumps(block, sort_keys=True).encode()
        # Return the hash using sha256
        return hashlib.sha256(block_string).hexdigest()

    """
    The proof of work algorithm is to find a number q such that 
    the hash of q x p (hash(pq)) contains 4 leading zeroes. 
    """
    def proof_of_work(self, last_block):
        last_proof = last_block['proof']
        last_hash = self.hash(last_block)
        proof = 0
        while self.valid_proof(last_proof, proof, last_hash) is False:
            proof += 1
        return proof

    @staticmethod
    # Static method to validate the proof
    def valid_proof(last_proof, proof, last_hash):
        guess = f'{last_proof}{proof}{last_hash}'.encode()
        guess_hash = hashlib.sha256(guess).hexdigest()
        return guess_hash[:4] == "0000"
