# Full Stack Blockchain Web App

This web application implements an Ethereum Marketplace where users can buy, auction, and sell items using Ethereum cryptocurrency and smart contracts using Metamask Ethereum wallets. 

## Tools Used:
* Ethereum 
* Solidity
* Ganache
* Truffle, Truffle Suite
* Metamask
* Javascript, npm, Node.js
* React/React-DOM
* HTML/CSS/TS

## Next Steps: 

* Implement more features in the Marketplace
* Support and allow multiple cryptocurrencies 
* Improve performance and UI
* Augment server and frontend management 
