// Import components
import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css'
import App from './components/App';
import * as serviceWorker from './serviceWorker';

// Render the app by getting root js file
ReactDOM.render(<App />, document.getElementById('root'));

// This app was deployed locally to improve performance and allow dev tools and development
// To deploy globally, change to unregister()
serviceWorker.register();
