// Import solidity
pragma solidity ^0.5.0;

// Marketplace file for backend interaction with the React app and Ganache
contract Marketplace {
    // Instance variables of name and product class
    string public name;
    uint public productCount = 0;
    mapping(uint => Product) public products;

    // Class constructor for product
    struct Product {
        uint id;
        string name;
        uint price;
        address payable owner;
        bool purchased;
    }

    // Event constructor for creating a new product
    event ProductCreated(
        uint id,
        string name,
        uint price,
        address payable owner,
        bool purchased
    );

    // Event constructor for purchasing the product
    event ProductPurchased(
        uint id,
        string name,
        uint price,
        address payable owner,
        bool purchased
    );

    // Main constructor for React app
    constructor() public {
        name = "Full Stack Blockchain Marketplace";
    }

    // Function to create a new product given a name and price
    function createProduct(string memory _name, uint _price) public {
        // Require a valid name and price
        require(bytes(_name).length > 0);
        require(_price > 0);
        productCount++;
        // Create the product and trigger an event
        products[productCount] = Product(productCount, _name, _price, msg.sender, false);
        emit ProductCreated(productCount, _name, _price, msg.sender, false);
    }

    function purchaseProduct(uint _id) public payable {
        // Fetch the product and seller
        Product memory _product = products[_id];
        address payable _seller = _product.owner;
        //  Require a valid ID and price, and enough Ether
        require(_product.id > 0 && _product.id <= productCount);
        require(msg.value >= _product.price);
        require(!_product.purchased);
        // Require one is not selling to themselves
        require(_seller != msg.sender);
        // Transfer ownership to the buyer
        _product.owner = msg.sender;
        _product.purchased = true;
        products[_id] = _product;
        // Transfer the Ether to the seller
        address(_seller).transfer(msg.value);
        // Trigger an event
        emit ProductPurchased(productCount, _product.name, _product.price, msg.sender, true);
    }
}
