// Import solidity
pragma solidity >=0.4.21 <0.6.0;

// Migrations file (called by initial migration)
contract Migrations {
  // Initialize instance variables
  address public owner;
  uint public last_completed_migration;

  // Set the owner to the current user of the website
  constructor() public {
    owner = msg.sender;
  }

  // Set the owner object to be resctricted to only the user of the website
  modifier restricted() {
    if (msg.sender == owner) _;
  }

  // Test for a completed migration 
  function setCompleted(uint completed) public restricted {
    last_completed_migration = completed;
  }

  // Upgrade the migration and migrate to a new address
  function upgrade(address new_address) public restricted {
    Migrations upgraded = Migrations(new_address);
    upgraded.setCompleted(last_completed_migration);
  }
}
