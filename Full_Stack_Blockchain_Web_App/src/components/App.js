// Import React components, as well as the file dependencies
import React, { Component } from 'react';
import Web3 from 'web3'
import logo from '../logo.png';
import './App.css';
import Marketplace from '../abis/Marketplace.json'
import Navbar from './Navbar'
import Main from './Main'

// The app will deploy the navbar and main screens, as well as communicate with Solidity and Truffle
class App extends Component {

  // Load or mount the component with Web3 and load blockchain data from the smart contracts
  async componentWillMount() {
    await this.loadWeb3()
    await this.loadBlockchainData()
  }

  // Loading the components with web3
  async loadWeb3() {
    // Deploy the react screen based on the window type
    if (window.ethereum) {
      window.web3 = new Web3(window.ethereum)
      await window.ethereum.enable()
    }
    else if (window.web3) {
      window.web3 = new Web3(window.web3.currentProvider)
    }
    else {
      // Check for errors and prompt metamask
      window.alert('Non-Ethereum browser detected. Please enable Metamask.')
    }
  }

  // Load the blockchain data to the app
  async loadBlockchainData() {
    const web3 = window.web3
    // Load all accounts from Solidity
    const accounts = await web3.eth.getAccounts()
    this.setState({ account: accounts[0] })
    const networkId = await web3.eth.net.getId()
    const networkData = Marketplace.networks[networkId]
    // Check if network exists
    if(networkData) {
      const marketplace = web3.eth.Contract(Marketplace.abi, networkData.address)
      this.setState({ marketplace })
      const productCount = await marketplace.methods.productCount().call()
      this.setState({ productCount })
      // Load all products from Solidity
      for (var i = 1; i <= productCount; i++) {
        const product = await marketplace.methods.products(i).call()
        this.setState({
          products: [...this.state.products, product]
        })
      }
      this.setState({ loading: false})
    } 
    else {
      // Network not detected
      window.alert('Network not detected.')
    }
  }

  // Constructor for product creation
  constructor(props) {
    super(props)
    this.state = {
      account: '',
      productCount: 0,
      products: [],
      loading: true
    }
    this.createProduct = this.createProduct.bind(this)
    this.purchaseProduct = this.purchaseProduct.bind(this)
  }

  // Method to create the product given a name and price
  createProduct(name, price) {
    this.setState({ loading: true })
    this.state.marketplace.methods.createProduct(name, price).send({ from: this.state.account })
    .once('receipt', (receipt) => {
      this.setState({ loading: false })
    })
  }

  // Method to purchases the product given a name and a price
  purchaseProduct(id, price) {
    this.setState({ loading: true })
    this.state.marketplace.methods.purchaseProduct(id).send({ from: this.state.account, value: price })
    .once('receipt', (receipt) => {
      this.setState({ loading: false })
    })
  }

  // Render the app and load all features above
  render() {
    return (
      <div>
        <Navbar account={this.state.account} />
        <div className="container-fluid mt-5">
          <div className="row">
            <main role="main" className="col-lg-12 d-flex">
              { this.state.loading
                ? 
                <div id="loader" className="text-center"><p className="text-center">Loading...</p></div>
                : 
                <Main
                  products={this.state.products}
                  createProduct={this.createProduct}
                  purchaseProduct={this.purchaseProduct} 
                />
              }
            </main>
          </div>
        </div>
      </div>
    );
  }
}

// Export and deploy the React app 
export default App;
