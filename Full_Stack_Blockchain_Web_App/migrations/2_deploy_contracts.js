// Get marketplace artifacts
const Marketplace = artifacts.require("Marketplace");

// Deploy artifacts to Truffle
module.exports = function(deployer) {
  deployer.deploy(Marketplace);
};
