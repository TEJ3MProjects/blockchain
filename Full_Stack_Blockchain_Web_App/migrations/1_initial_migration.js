// Get migration artifacts 
const Migrations = artifacts.require("Migrations");

// Deploy artifacts to Truffle
module.exports = function(deployer) {
  deployer.deploy(Migrations);
};
